********************************************************************
*   Common f9dasm configuration for all disassemblies
*
*   • Errors in .info files will not be reported.
*   • This modeline may be useful when reading the `.dis` buffers in Vim:
*       vim: set autoread iskeyword-=-
*   • See the Header Comments section below for code conventions.
*
*   Reference:
*   https://htmlpreview.github.io/?https://github.com/Arakula/f9dasm/blob/master/f9dasm.htm
*

********************************************************************
*   Disassembly options

option 6802
option noconv
option cchar ;

*   These disable the address, hexdump and ASCII dump in comments to
*   the right of the disassembled code.
option noaddr
option nohex
option noasc

********************************************************************
*   Header Comments

comment     0000    ----------------------------------------------------------------------
comment     0000   . 
comment     0000   .   Naming conventions:
comment     0000   .     _####     local branch; woud be local label in assemblers that support it
comment     0000   .     _rts_#### local branch to RTS instruction
comment     0000   .     U.####    unknown I/O port at address ####
comment     0000   . 
comment     0000

********************************************************************
*   Global Variables

********************************************************************
*   I/O

********************************************************************
*   Code

option offset 4000

*   XXX Not totally sure about this, but it definitely doesn't look like
*   code, more like fonts or something like that. We'll come back to this
*   once we've determined some other data areas which may be what are
*   making code references into this.
data        4000-7FFF

data        8000-803F

comment     8040
comment     8040    ----------------------------------------------------------------------
comment     8040   .   Entry point from reset vector
comment     8040
label       8040    reset

*   These are fairly conservative estimates of the data covering only what
*   is obviously text and starting at whatever M.xxxx symbol we find just
*   before it, where obvious. Annoyingly, they appear to be using high bit
*   termination of strings.
data        920A-9272
data        982A-A463
data        B7EA-C141
data        C94E-CA11

comment     F97F
data        F97F-FFEF   * all-zeros

*************************************************************************
*   We need to redefine all the symbols for the vectors (replacing the
*   auto-generated svec_RST/hdlr_RST etc. etc. names) so that we use our
*   own names for locations to which these vectors point. This is just
*   a workaround for a bug with the disassembler, which would otherwise
*   ignore our redefinition of the label for F800 above.


comment     FFF0
comment     FFF0    ----------------------------------------------------------------------
comment     FFF0    Interrupt vectors
comment     FFF0    Only IRQ, SWI and RESET are used, as far as we can tell.
comment     FFF0

word        FFF0-FFF1
lcomment    FFF0-FFF1   accidental default in reserved vector?

cvector     FFF2-FFFF
label       FFF2    SWI3vec
label       FFF4    SWI2vec
label       FFF6    FIRQvec
label       FFF8    IRQvec
label       FFFA    SWIvec
label       FFFC    NMIvec
label       FFFE    RSETvec
