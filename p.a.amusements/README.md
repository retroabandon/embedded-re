P.A. Amusements Machine
=======================

Board from a dual-CPU 6809 machine from P.A. Amusements.

The machine has several ROMs:
- `game.bin` U57: Presumed to be the main 6809 code to run the game.
  This dump drops the first 16K ($0000-$3FFF); that's programmed all
  to $00 and clearly not mapped into the address space. (Instead RAM
  and devices are mapped into that area.)
- Graphics ROMs U25-U28.
- Character ROM U29.
- Account ROM.
