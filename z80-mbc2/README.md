Z80 MBC2 Code
=============

The [Z80 MBC2] is a single-board Z80-based computer that uses an Atmel
ATmega32A (with Arduino bootloader) to emulate all the I/O. It's not
well documented; the only low-level documentation appears to be the source
code for [IOS-LITE], which is an Arduino sketch for the ATmega code which
has all the Z80 code embedded in it as binary blobs. The full [IOS], which
includes SD-card disk support, appears to be closed source.

Contents:
- `iload.c` contains the Z80 object code for the "iLoad" Intel hex
  bootloader, copied directly from the IOS-LITE source.



<!-------------------------------------------------------------------->
[Z80 MBC2]: https://github.com/SuperFabius/Z80-MBC2/
[IOS-LITE]: https://github.com/SuperFabius/Z80-MBC2/blob/master/S220618_IOS-LITE-Z80-MBC2.ino
[IOS]: https://github.com/SuperFabius/Z80-MBC2/blob/master/S220718-R260119_IOS-Z80-MBC2.ino.with_bootloader_atmega32_16000000L.hex
