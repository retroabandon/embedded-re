Embedded Systems Reverse Engineering
====================================

- [`Moseley-MRC-2/`]: Reverse-engineering a 6809-based parallel to serial
  board posted on Retrocomputing.StackExchange.com. It turned out to be
  part of a Moseley MRC-2 system.
- [`ex-2000-drive-tester`]: A 6502-based disk drive tester.
- `k-t-anzeige`: An old stand-alone message display
- [`p.a.amusements`]: 6809 based board from a game machine.
- [`z80-mbc2/`]: RE of bits of the SuperFabius [Z80 MBC2] code.


<!-------------------------------------------------------------------->
[`Moseley-MRC-2/`]: ./Moseley-MRC-2/
[`ex-2000-drive-tester`]: ./ex-2000-drive-tester/
[`p.a.amusements`]: ./p.a.amusements/
[`z80-mbc2`]: ./z80-mbc2/

[Z80 MBC2]: https://github.com/SuperFabius/Z80-MBC2/
