This is [cjs's answer][answer] to the Retrocomputing StackExchange
question, ["Can anyone identify this Parallel to Serial Interface
Board?"][rc 12501].

[rc 12501]: https://retrocomputing.stackexchange.com/q/12501/7208
[answer]: https://retrocomputing.stackexchange.com/a/13607/7208

----------------------------------------------------------------------


The boards are essentially "serial to parallel converters," but
probably not of any type anybody here is familiar with. A
[manual][mrc2-2] has been found that describes two devices, the MSD-1
Multiple Status Display and the MDC-2 Multiple Direct Command Option,
used with the Moseley Associates Inc. MRC-2 system, a
microprocessor-based system for monitoring and control of equipment
such as radio and TV transmitters.

### Hardware

The board is a fairly basic SBC, with a 6809 CPU, 2 KB ROM, 2 KB RAM,
a 6850 ACIA for serial input and some parallel I/O.

On one of the DB-25 connectors the the system receives RS-232 serial
input at 300 or 1200 bps (based on the switch setting). I am guessing
that this serial signalling is passed through (via hardware, not an
ACIA) to the other DB-25 connector for connection to further similar
boards so that they can see the same data stream. The firmware itself
never generates any serial output.

The board's output is 32 digital signals on the DB-37 connector.
Presumably these signals were used to control something, but there's
no indication of what. The output is handled by a pair of 6821 PIAs,
U25 and U26, together referred to as "P32" in the disassembly.

There is a third PIA, U27 ("P16" in the disassembly) that is used for
things like reading the switches and signalling that the system is
alive and/or restarting.

### Function

The board receives a stream of characters on the serial port and,
based on what it receives, sets bits on the 32-bit digital output.
This appears to be designed so that one stream of serial data can be
passed to multiple boards; each one has an address from 0-15 (probably
set by the rotary switch) that determines which characters it will
take as commands to change its digital output and which it will
ignore.

The protocol is not like anything I've seen before and is probably
proprietary. It's not a simple case of specific characters toggling
specific bits on the digital output (e.g., "set bit 5"), but instead
there's some sort of obscure processing determining how the outputs
are set.

Without knowing the purpose of the board the protocol itself doesn't
seem terribly interesting, but you can read through the state machine
code that processes the input and updates the output if you want to
start to dig into the details.

### Alternative Designs

As Ross Ridge mentions in the comments on the question, yes, this
board is a bit overkill on the hardware side. The hardware couldn't
reasonably be as simple as just an ACIA and 74xx logic, given that it
needs to interpret (and, for the switch unit, generate) a standard
serial protocol used by various other devices, but certainly cheaper
CPU, RAM and PIA options could have been used.

From the filename of the catalogue below, the system was designed no
later than 1981. I probably would have suggested a [6508]
microprocessor, which included 256 bytes of RAM (plenty for this
application) and an 8-bit PIA. Five bits of the PIA would be used for
input from the switches and the other three for shift registers
(74LS595 or similar) for the data outputs and switch inputs. Using
pricing from an [advert in the February 1981 issue of BYTE
Magazine][byte-8102-374], this would have replaced the $38 6809 with
an MPU possibly even cheaper than the $12 6502 and completely dropped
three 6821 PIAs at $6.50 each, for something like a $50 saving on the
BOM.

That said, I suspect that these boards were used in other (possibly
more sophisticated) designs as well, and the code is clearly re-using
parts of a standard framework for more sophisticated systems. (See how
the interrupt-handling code works, for example.) So it's probably not
unreasonable to re-use existing, tested resources even at the cost of
a more expensive BOM.

### Resources

The [MRC-1 Catalogue][mrc-1] gives an overview of the full system in
which the accessories containing these boards were used. [MRC 2
Microprocessor Remote Control System, Volume One][mrc2-1] is the
manual for that system, and [MRC 2 Microprocessor Remote Control
Options, Volume Two][mrc2-2] is the manual for the products that used
these boards. You can see a picture of an installation including the
MRC 2 and these two options (at the lower right) at [Alice@97.3
Transmitter][alice].

The [rcse-mystery-board][repo] repository on GitLab contains
high-resolution photos of the board, a dump of the ROMs (two, since
they were somewhat different between the two examples of this board) a
disassembler and framework to run it, and an info file for the
disassember that produces a very heavily commented, but not entirely
complete, [disassembly of one ROM][dis]. (There's also a bit of
disassembly of the other ROM, particularly the common parts.)

The README gives details of what's there and how to use the tools, and
if you walk foward through the commit history from the start you'll
see how I built the tools and did the disassembly, step by step. (The
commit messages generally contain detailed commentary on this.)


[6508]: https://en.wikipedia.org/wiki/MOS_Technology_6502#Variations_and_derivatives
[byte-8102-374]: https://archive.org/details/eu_BYTE-1981-02_OCR/page/n374
[mrc-1]: http://www.steampoweredradio.com/pdf/moseley/catalogs/Moseley%20MRC-1%20Remote%20Control%20Brochure%20March%201981.pdf
[mrc2-1]: http://www.steampoweredradio.com/pdf/moseley/manuals/Moseley%20MRC-2%20Microprocessor%20Remote%20Control%20System%20Manual%20Volume%201.pdf
[mrc2-2]: http://www.steampoweredradio.com/pdf/moseley/manuals/Moseley%20MRC-2%20Microprocessor%20Remote%20Control%20System%20Manual%20Volume%202.pdf
[repo]: https://gitlab.com/retroabandon/rcse-mystery-board.git
[dis]: https://gitlab.com/retroabandon/rcse-mystery-board/-/blob/master/PS-1.dis
[alice]: http://www.steampoweredradio.com/New%20Tour/alice/fmx4.html
