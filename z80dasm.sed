#
#   z80dasm.sed - reformat z80dasm output to make it more readable
#
#   Version: 0.1.0
#
#   This is generally called by piping the `z80dasm -a` output into the
#   following, where `foo.bin` is the file being disassembled and `foo.sed`
#   below are the annotations particular to that file. Note that the extra
#   sed command to remove trailing whitespace must be separate in order to
#   handle annotations that introduce new lines into the file; using the
#   same `sed` will not remove trailing whitespace from any lines
#   preceeding the last new line added by a single sed command.
#
#     | sed -E -f ../z80dasm.sed -f foo.sed \
#     | sed -E -e 's/[ \t]+$//  # remove any trailing whitesapce' \
#     | expand > foo.dis
#

/^[^\t;].*:$/{N;s/\n//}             # join labels to next line
s/l([0-9a-f]{4})h/unk\U\1/g         # more readable label format: unkXXXX
s/sub_([0-9a-f]{4})h/sub_\U\1/g
s/0x([0-9a-f]{4})/$\U\1/            # number format 0xnnnn → $NNNN
s/0([0-9a-f]{4})h/$\U\1/            # number format 0nnnnh → $NNNN
s/0([0-9a-f]{2})h/$\U\1/            # number format 0nnh → $NN
s/\t[a-z][a-z] /&  /                # indent operand after 2-letter mnemonic
s/\t[a-z]{3} /& /                   # indent operand after 3-letter mnemonic
s/^\t/\t    /                       # indent non-label lines
s/:\t/\t    /                       # indent menmonic on label lines, remove :
