Klartextanzeige AN2001.C
========================

This is a stand-alone 16-character by 2-line LED character display
that displays messages programmed into ROMs inside the unit.

It's owned by `tomatolicious` on the Usagi Electric server on Discord.

`anzeige.bin` is the program ROM and `text.bin` is the one message ROM
that was in the system.

Hardware
--------

ICs:
- MC6802P (DIP-40W): CPU
- 2× [MC6821P][] (DIP-40W): Peripheral Interface Adapter (PIA)
  - Pinout below.
- D2732 EPROM (DIP-24W): program code
- 4× ZIF DIP-24W EPROM sockets for 2732 devices containing message data
- MC74HC138N (DIP-16): 3→8 decoder/demux, inverting out [SN74LS138]
  - Pins: `A B C G̅2̅A̅ G̅2̅B̅ G1 Y̅7 GND ‥ Y̅6 Y̅5 Y̅4 Y̅3 Y̅2 Y̅1 Y̅0 Vcc`
  - enable: G1, G̅2̅A̅, G̅2̅B̅  (disabled: all outputs high)
  - input: A (LSB), B, C
  - output: one of Y0...Y7 low: ABC=0→Y0, ABC=1→Y1, ..., ABC=7→Y7
- 6× ILD74 L8614H (DIP-8N): optocouplers

MC6821P pinout:


     CA1 CA2 I̅R̅Q̅A̅ I̅R̅Q̅B̅ RS0 RS1 R̅S̅E̅T̅ D0  D1  D2  D3  D4  D5  D6  D7 Eϕ2 CS1 C̅S̅2 CS0 R/W̅
    ┌──────────────────────────────────────────────────────────────────────────────────┐
    │ 40  39  38   37  36  35   34  33  32  31  30  29  28  27  26  25  24  23  22  21 │
    │● 1   2   3    4   5   6    7   8   9  10  11  12  13  14  15  16  17  18  19  20 │
    └──────────────────────────────────────────────────────────────────────────────────┘
      Vss PA0 PA1  PA2 PA3 PA4 PA5 PA6 PA7 PB0 PB1 PB2 PB3 PB4 PB5 PB6 PB7 CB1 CB2 Vcc

DIP switches (8):
- Current settings: 0000.0010 (1=on)

Front Displays:
- Agilent [HPDL-1414] Four Character Smart Alphanumeric Displays
  - Pinout: `D₆ D₄ W̅R̅ A₁ A₀ Vdd ‥ D₅ D₃ D₂ D₁ D₀ GND`
  - Buffers and displays ASCII characters $20-$5F, presented on D₆-D₀.
    (Blank for $00-$1F and $60-$7F.)
  - A₀/A₁ select address of character, left to right 3,2,1,0.
  - Bring `W̅R̅` low to load character into given address.

Back Panel has 16 connectors:

      +  -  -  ⊥ C2 C1 2⁹ 2⁸ 2⁷ 2⁶ 2⁵ 2⁴ 2³ 2² 2¹ 2⁰
      1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16

- 1/`+`, 2`-`: input power DC 18-32 V (?) 0.63 A


6800 CPU
--------

The 6802 is a [Motorola 6800] with a slightly simpler hardware interface
(onboard clock generator) and 128 bytes of onboard RAM. The registers are:

    A   8-bit accumulator. Primarily used for loads/stores and arithmetic.
    B   8-bit accumulator. Often used as a loop counter.
    X   16-bit index register. Instructions that load/store through it include
        an 8-bit offset ($00-$FF) from X with the instruction as "n,X".
    SP  Stack pointer; loaded and stored with LDS/STS;
        transferred to/from X register with TSX/TXS.

The flags stored in the processor status byte as `11HINZVC` are:

    1   Always 1 on read; ignored by by TAP, RTI, etc.
    H   Half carry: set on b3→b4 carry for ADD, ABA, ADC
    I   Interrupt mask; IRQ masked when set.
    N   Negative: high order bit of result
    Z   Zero: checked result == 0
    V   Overflow
    C   Carry-borrow (carry for add; borrow for subtract).

`CMP A,mem`/`SUB A,mem` carry flag behaviour is opposite of 6502:
- mem > A  =  A < mem  =  1 set     (`BCS` is an unsigned "BLE")
- mem ≤ A  =  A ≥ mem  =  0 clear   (`BCC` is an unsigned "BGT")

As well as the usual `BEQ`/`BNE` etc. for the single flags above, there is:

`CMP`,`SUB` carry flag is opposite of 6502:
- Unsigned: `BLS` (less-than/same) `BHI`
- Two's complement: `BLT` `BLE` `BGE` `BGT`


Firmware
--------

Memory Map:

    0000-007F 128b  Processor internal RAM 9128
    0080-00FF 128b  PIA #1 (assumed from code)
    0100-017F 128b  PIA #2 (assumed from code)
    F800-FFFF   4K  Firmware ROM



<!-------------------------------------------------------------------->
[HPDL-1414]: https://www.farnell.com/datasheets/76528.pdf
[MC6821P]: https://github.com/0cjs/sedoc/blob/master/ee/mc6820.md
[Motorola 6899]: https://en.wikipedia.org/wiki/Motorola_6800
[SN74LS138]: http://www.ti.com/lit/gpn/sn74ls138
